#!/usr/bin/env python3
"""Download data from the parkrun website, storing it in data.txt"""

import re
import time
import random
import xml.etree.ElementTree
from dataclasses import dataclass
import urllib
import requests


@dataclass
class Result:
    url: str
    position: str
    gender: str
    gender_pos: str
    age_cat: str
    club: str
    time: str
    is_pb: str
    age_grade: str
    cat_pos: str


with open("parkrunners.csv", "r") as f:
    runner_names = f.read().split("\n")

while "" in runner_names:
    runner_names.remove("")

runner_names = [row.strip(",").split(",")[1:] for row in runner_names]
runner_names = {row[0]: [None] if row[1] == "None" else row[1:] for row in runner_names}


def write_file(filename, text):
    """Save a string to a file"""
    with open(filename, "w") as f:
        f.write(text)


def load_list(filename):
    """Load a newline-separated list from a text file"""
    with open(filename, "r") as f:
        lst = f.read().split("\n")
    while "" in lst:
        lst.remove("")
    return lst


dates = load_list("dates.txt")
PARKRUNS = load_list("counters.txt")

HEADERS = {"User-Agent": "API workaround"}

STARTTABLE = '<table class="Results-table Results-table--compact js-ResultsTable">'
ENDTABLE = "</table>"
DELAY = (7, 8)  # (Mindelay, maxdelay)
GOODTEXT = "Penistone Footpath Runners AC"

BBL = "http://www.parkrun."
BASELINK = BBL + "org.uk/"

RUNNERLINK = BASELINK + "results/athleteresultshistory/?athleteNumber="
URL = BBL + "com/results/consolidatedclub/?clubNum=2801&eventdate="

with open("year.txt", "r") as f:
    YEAR = f.read().strip(" \n")


def string_cut(text, start, end):
    text = text[text.find(start) + len(start) :]
    return text[: text.find(end)]


def percentage(amount, total, places=1):
    """Convert a value to a string percentage"""
    value = 100 * amount / total
    if places is None:
        return str(value) + "%"
    return str(round(value, places)) + "%"


def find_links(text):
    """Locate the links in a HTML file for the club summary page"""
    key = "/results/weeklyresults/?runSeqNumber="
    links = []
    while True:
        p = text.find(key)
        if p == -1:
            break  # No match

        # Search backwards and forwards from the key to find the whole
        # URL

        # Guaranteed to be at least this far back
        start = p - 25
        while text[start] != "'":
            start -= 1

        # Guaranteed to be at least this far forwards
        end = p + len(key)

        while text[end] != "'":
            end += 1
        link = text[start + 1 : end]
        links.append(link)
        text = text.replace(link, "")

    return links


def get_page(url):
    """Get the HTML code, as a string, from a website"""
    request = urllib.request.Request(url, data=None, headers=HEADERS)
    while True:
        try:
            r = urllib.request.urlopen(request)
            break
        except urllib.error.URLError:
            print("Error: retrying")
            randomdelay(DELAY)
            continue
    if not isinstance(r, str):
        r2 = r.read()
        r.close()
        r = r2

    try:
        n = r.decode()
    except UnicodeDecodeError:
        print("ERROR: " + str(url))
        n = ""
    return n


def find_table(text):
    """Find the useful table and return it"""
    if not text:
        return ""
    loc = text.find(STARTTABLE)
    text = text[loc + len(STARTTABLE) :]
    loc = text.find(ENDTABLE)
    text = text[:loc]
    return "<all>" + text + "</all>"


def remove_image_tags(data):
    """Find and remove HTML img tags from the HTML"""
    p = re.compile(r"<img .*?>")
    return p.sub("", data)


def randomdelay(delay):
    time.sleep(random.uniform(DELAY[0], DELAY[1]))


def parse(string):
    """Go through an HTML string and create a nice table, with only the
       wanted runners"""

    string = remove_image_tags(string)
    # Replace <br> tags with spaces
    string = string.replace("<br/>", " ").replace("<br>", " ").replace("<br />", " ")
    # Get tree
    root = xml.etree.ElementTree.fromstring(string)[1]

    # Convert the HTML to a namedtuple list
    table = []
    for result in root:
        name = result.attrib["data-name"]
        if name == "Unknown":
            continue
        position = int(result.attrib["data-position"])
        try:
            gender = result.attrib["data-gender"][0]
        except IndexError:
            continue
        age_cat = result.attrib["data-agegroup"]
        club = result.attrib["data-club"]
        age_grade = result.attrib["data-agegrade"]
        is_pb = result.attrib["data-achievement"]
        gender_pos = int(result[2][1].text)
        run_time = result[5][0].text
        url = result[1][0][0].attrib["href"].split("=")[-1]
        table.append(
            Result(
                url,
                position,
                gender,
                gender_pos,
                age_cat,
                club,
                run_time,
                is_pb,
                age_grade,
                None,
            )
        )

    # Calculate age position
    runnersbefore = dict()
    for runner in table:
        agecat = runner.age_cat
        if agecat in runnersbefore.keys():
            runnersbefore[agecat] += 1
            runner.cat_pos = runnersbefore[agecat]
        # Runner is first in age category
        else:
            runner.cat_pos = 1
            runnersbefore[agecat] = 1

    # Filter out the runners that are from the wrong club
    table = list(filter(lambda r: r.club == GOODTEXT, table))

    # Update links data
    nld = {}
    for number in (x.url for x in table):
        if number in runner_names and runner_names[number][0] is not None:
            # Add the runner's link to the dict
            nld[runner_names[number][0]] = str(number)

    oldlinks.update(nld)

    # Make PBs binary
    for run in table:
        if run.is_pb == "New PB!":
            run.is_pb = 1
        else:
            run.is_pb = 0

        run.age_grade = run.age_grade.replace(" %", "")

    # Convert to table
    table = [
        [
            r.url,
            r.time,
            r.age_cat,
            r.age_grade,
            r.gender,
            r.gender_pos,
            r.is_pb,
            r.cat_pos,
        ]
        for r in table
    ]
    return table


def main():
    global oldlinks

    # Open the existing data file to see which dates have been done
    try:
        with open("data.txt", "r") as f:
            table = eval(f.read())[0]
    except FileNotFoundError:
        table = []

    donedates = set()
    for row in table:
        donedates.add(row[2])  # The date field for each race

    # Find the URLs of the runner's parkrun pages
    try:
        with open("Runner_Links.txt", "r") as f:
            oldlinks = eval(f.read())
    except FileNotFoundError:
        oldlinks = {}

    # Actually start to download the pages
    for date in dates:
        if date in donedates:
            continue
        html = get_page(URL + date)
        links = find_links(html)

        for link in links:
            # Foreign parkruns do not count
            if "org.uk" not in link:
                continue
            html = get_page(link)
            results = parse(find_table(html))
            header = string_cut(html, '<div class="Results-header">', "</div>")
            parkrunname = string_cut(header, "<h1>", "</h1>").replace(" parkrun", "")
            num = int(string_cut(header, "#", "</span>"))
            results = [[parkrunname, num, date] + row for row in results]

            table += results
            randomdelay(DELAY)
            print(
                percentage(
                    dates.index(date) + links.index(link) / len(links), len(dates)
                )
            )

    # Write the new links
    write_file("Runner_Links.txt", repr(oldlinks))

    # Calculate the volunteering times
    volunteerpoints = {}
    totalparkruns = {}
    for i, link in enumerate(oldlinks.keys()):
        time.sleep(random.uniform(DELAY[0], DELAY[1]))
        print(percentage(i, len(oldlinks)))
        theirpage = get_page(RUNNERLINK + oldlinks[link])

        # Get total parkruns
        totalbit = theirpage[: theirpage.find(" parkruns")][::-1]
        totalbit = int(totalbit[: totalbit.find("(")][::-1])
        totalparkruns[link] = totalbit

        if "Volunteer Summary" not in theirpage:
            volunteerpoints[link] = 0
            continue

        # Narrow down on the table body
        theirpage = theirpage[theirpage.find("Role") + 53 :]
        theirpage = theirpage[: theirpage.find("</tbody")]
        vpoints = 0
        while YEAR in theirpage:
            theirpage = theirpage[theirpage.find(YEAR) + 13 :]
            theirpage = theirpage[theirpage.find("</td><td>") + 9 :]
            volunteered = int(theirpage[: theirpage.find("<")])
            vpoints += volunteered

        volunteerpoints[link] = vpoints

    texttowrite = repr([table, volunteerpoints, totalparkruns])
    write_file("data.txt", texttowrite)


if __name__ == "__main__":
    main()
